package xiaofei.library.hermestest;

import android.app.Application;

import xiaofei.library.hermes.Hermes;
import xiaofei.library.hermestest.service.FileUtils;
import xiaofei.library.hermestest.service.LoadingCallback;
import xiaofei.library.hermestest.service.LoadingTask;
import xiaofei.library.hermestest.service.UserManager;
import xiaofei.library.hermestest.test.C;
import xiaofei.library.hermestest.test.NewInstance;

/**
 * @author xuexiang
 * @since 2018/8/9 下午5:39
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Hermes.init(this);
        Hermes.register(NewInstance.class);
        Hermes.register(C.class);
        Hermes.register(UserManager.class);
        Hermes.register(LoadingTask.class);
        Hermes.register(FileUtils.class);
        Hermes.register(LoadingCallback.class);
    }
}
