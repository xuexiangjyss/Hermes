package xiaofei.library.hermestest.service;

import xiaofei.library.hermes.annotation.ClassId;

/**
 * Created by Xiaofei on 16/4/25.
 */
@ClassId("LoadingCallback")
public interface LoadingCallback {

    void callback(int progress);
}
