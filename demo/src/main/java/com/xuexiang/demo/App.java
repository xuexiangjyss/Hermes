package com.xuexiang.demo;

import android.app.Application;

import com.xuexiang.demo.service.IFileUtils;
import com.xuexiang.demo.service.ILoadingTask;
import com.xuexiang.demo.service.IUserManager;
import com.xuexiang.demo.service.LoadingCallback;

import xiaofei.library.hermes.Hermes;

/**
 * @author xuexiang
 * @since 2018/8/9 下午5:53
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Hermes.init(this);
        Hermes.register(IUserManager.class);
        Hermes.register(ILoadingTask.class);
        Hermes.register(IFileUtils.class);
        Hermes.register(LoadingCallback.class);
    }
}
